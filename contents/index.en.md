---
title: Tentang Bizzy
banner: https://assets.bizzy.co.id/static-page/tentang-kami@3x.jpg
bannerTitle: Bizzy.co.id adalah marketplace digital, dinamis dan inklusif <br/>untuk bisnis yang dilengkapi oleh platform e-procurement <br/>untuk proses pencarian (sourcing), pengadaan taktis dan transaksional.
date: 2012-10-01 15:00
locale: en
template: index.jade
filename: en/index.html
---

<div class="container-about">
  <div>
    <div class="menu-body uk-text-center"><a  href="/static-page/about" class="user-link nuxt-link-exact-active nuxt-link-active active">Tentang Kami</a><a  href="/static-page/our-team" class="user-link">Tim Kami</a><a  href="/static-page/career" class="user-link">Karir</a><a  href="/static-page/cooperation-status" class="user-link">Status Kerja Sama</a><a  href="/static-page/media-reviews" class="user-link">Ulasan Media</a></div>
    <hr  class="menu-separate">
  </div>
  <div class="title-body">
    <h3 >Nilai Utama</h3>
  </div>
  <div class="container-one">
    <div class="uk-grid uk-grid-match uk-grid-large list-set uk-child-width-1-2@m">
      <div>
        <div class="uk-grid set">
          <div class="uk-width-5-5 uk-width-1-5@m"><img  src="https://assets.bizzy.co.id/static-page/about/transparancy.svg" alt=""></div>
          <div class="uk-width-5-5 uk-width-4-5@m">
            <div class="title-box1"><span >Perdagangan yang transparan, efisien, <br>dan dapat dipertanggungjawabkan adalah landasan kami</span></div>
            <div class="uk-margin-small-top uk-margin-bottom"><span ><b>Sebuah perjalanan seribu mil dimulai dengan satu langkah.</b></span></div>&#x9;&#x9;&#x9;&#x9;&#x9;Kami percaya untuk mencapai perekonomian yang bersih melalui transparansi, efisiensi dan akuntabilitas, satu perdagangan dalam satu waktu.
          </div>
        </div>
      </div>
      <div>
        <div class="uk-grid set">
          <div class="uk-width-5-5 uk-width-1-5@m"><img  src="https://assets.bizzy.co.id/static-page/about/proud.svg" alt=""></div>
          <div class="uk-width-5-5 uk-width-4-5@m">
            <div class="title-box1">Bangga atas keahlian</div>
            <div class="uk-margin-small-top uk-margin-bottom"><b > Ide itu murah, eksekusi adalah yang terpenting. </b></div>&#x9;&#x9;&#x9;&#x9;&#x9;Kami melakukan segalanya dengan kemampuan terbaik.
          </div>
        </div>
      </div>
    </div>
    <div class="uk-grid uk-grid-match uk-grid-large uk-margin-large-top list-set uk-child-width-1-2@m">
      <div>
        <div class="uk-grid set">
          <div class="uk-width-5-5 uk-width-1-5@m"><img  src="https://assets.bizzy.co.id/static-page/about/moveon.svg" alt=""></div>
          <div class="uk-width-5-5 uk-width-4-5@m">
            <div class="title-box1">Selalu bergerak maju adalah pilihan kami satu-satunya</div>
            <div class="uk-margin-small-top uk-margin-bottom"><b >Kami menerima dan menghadapi tantangan yang ada sekarang.</b></div>&#x9;&#x9;&#x9;&#x9;&#x9;Kami menyadari bahwa satu-satunya hari yang mudah dilalui adalah kemarin, oleh karena itu kami mungkin sering gagal. Namun kami tidak akan berhenti hingga kami berhasil karena kegagalan bukanlah pilihan. Kegagalan hanya suatu keadaan pemikiran.
          </div>
        </div>
      </div>
      <div>
        <div class="uk-grid set">
          <div class="uk-width-5-5 uk-width-1-5@m"><img  src="https://assets.bizzy.co.id/static-page/about/balance.svg" alt=""></div>
          <div class="uk-width-5-5 uk-width-4-5@m">
            <div class="title-box1">Inovasi dan perbaikan berkelanjutan melalui kolaborasi</div>
            <div class="uk-margin-small-top uk-margin-bottom"><b >Inovasi adalah kekuatan kami, dan kekuatan kami adalah kekuatan masing-masing individu. Dengan inovasi, perbaikan tidak memiliki batas akhir.</b></div>&#x9;&#x9;&#x9;&#x9;&#x9;Kami percaya pada kolaborasi menyeluruh dan mendalam dalam tim untuk terus melakukan inovasi dan perbaikan berkelanjutan. Mengalahkan kompetisi relatif mudah, namun mengalahkan diri sendiri membutuhkan komitmen tiada akhir.
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-2">
    <div class="uk-grid uk-grid-medium list-set">
      <div class="uk-width-2-5 set"><img  src="https://assets.bizzy.co.id/static-page/about/graph.svg" alt=""></div>
      <div class="uk-width-3-5 set">
        <div class="title-section uk-margin">Misi</div>&#x9;&#x9;&#x9;Bizzy.co.id memiliki misi untuk mendukung perekonomian yang bersih dengan menggerakan ekosistem bisnis digital yang inklusif yang memungkinkan perdagangan yang transparan, efisien, dan akuntabel bagi semua pemegang kepentingan.
      </div>
    </div>
  </div>
  <div class="container-2">
    <div class="why-us-box">
      <div class="uk-grid uk-grid-medium list-set">
        <div class="uk-width-2-5 set"><img src="https://assets.bizzy.co.id/static-page/about/why.svg" alt=""></div>
        <div class="uk-width-3-5 set">
          <div class="title-section">Menagapa Kami</div>
          <div class="uk-margin-medium-top uk-margin-medium-bottom">Bizzy.co.id menghubungkan vendor dengan pelanggan bisnis, mulai dari UKM hingga korporasi dalam satu platform yang mempermudah seluruh proses pengadaan untuk perusahaan,  mulai dari pencarian cepat (Quick Search), bandingkan harga grosir bertingkat (Compare Tier Price), pilih penjual (Select Merchant), pembayaran secara mudah (Easy Pay), dan cek status pengiriman (Shipment Tracking).</div>&#x9;&#x9;&#x9;&#x9;Bizzy.co.id menghadirkan fitur-fitur yang khusus menjawab kebutuhan B2B yaitu harga grosir bertingkat sesuai jumlah pembelian (Tier Price), pilihan beberapa vendor untuk 1 sku produk dengan peringkat atas performa (Multiple Vendor with Rank), alur persetujuan (Approval Flow), faktur pajak online (e-Faktur), serta gratis biaya pengiriman ke kantor untuk area Jadetabek (Free Delivery).
        </div>
      </div>
    </div>
  </div>
</div>
