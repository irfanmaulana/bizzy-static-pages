
# Bizzy Static Pages

A static pages build in top of Wintersmith

## Development

**Install dependencies**

```bash
yarn install
```

**For development**

```bash
yarn run dev
```

**Build file production**

```bash
yarn run build
```

## Page sample

+ [http://localhost:8080/](http://localhost:8080/) - ID Languange
+ [http://localhost:8080/en](http://localhost:8080/en) - EN Language

## Read about wintersmith 
 
+ [https://github.com/jnordberg/wintersmith](https://github.com/jnordberg/wintersmith)

## Copyright

© 2018